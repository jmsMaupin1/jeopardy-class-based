class JeopardyGrid extends Grid {
    constructor(options, questions = {}) {
        super(options);
        this.playerScore = 0;
        this.questions = questions;
        this.currentQuestionAndAnswer = {};
    }

    createCellInstance() {
        return new JeopardyCell(this.options)
    }

    addCategoryHeader(cellIndex, category) {
        const cell = this.matrix[0][cellIndex];
        const cellElement = cell.cell;

        cellElement.textContent = category;
    }

    addQuestionsToCell({rowIndex, cellIndex, value, question, answer}) {
        const cell = this.matrix[rowIndex][cellIndex];
        cell.value = value;
        cell.question = question;
        cell.answer = answer;
        const cellElement = cell.cell;

        cellElement.textContent = cell.value;
    }

    removeQuestionFromCell(rowIndex, cellIndex) {
        const cell = this.matrix[rowIndex][cellIndex];
        cell.value = 0;
        cell.question = "";
        cell.answer = "";

        cell.cell.textContent = "";
    }

    chooseClue(event) {
        let {rowIndex, cellIndex} = event.target.dataset;
        let cell = this.matrix[rowIndex][cellIndex];

        if (!cell.isEmpty()) return;
        
        let QnAModal = document.getElementById('qna')
        let question = document.getElementById('question');
        let answer   = document.getElementById('answer');

        this.currentQuestionAndAnswer = { 
            question: cell.question, 
            answer: cell.answer, 
            value: cell.value,
            rowIndex: rowIndex,
            cellIndex: cellIndex
        };

        question.textContent = cell.question;
        QnAModal.style.display = 'flex'

        console.log(this.matrix[rowIndex][cellIndex].question);
        console.log(this.matrix[rowIndex][cellIndex].answer)
    }

    checkAnswer(answer) {
        if(answer.toLowerCase() === this.currentQuestionAndAnswer.answer.toLowerCase()) {
            this.playerScore += this.currentQuestionAndAnswer.value;
            document.getElementById('score').innerHTML = `<h1>${this.playerScore}</h1>`
        }

        this.removeQuestionFromCell(
            this.currentQuestionAndAnswer.rowIndex,
            this.currentQuestionAndAnswer.cellIndex
        )
    }
}