class Cell {
    constructor(options) {
        this.options = options;
        this.options.cellClasses = this.options.cellClasses || [];
    }

    createCellElement(rowIndex, cellIndex) {
        this.cell = document.createElement('div');
        this.addStyleClass('grid-cell', ...this.options.cellClasses); 
        this.cell.style.height = this.options.cellHeight;
        this.cell.style.width = this.options.cellWidth;
        this.cell.dataset.rowIndex = rowIndex;
        this.cell.dataset.cellIndex = cellIndex;

        return this.cell;
    }

    addStyleClass(...classes) {
        this.cell.classList.add(...classes);
    }
}