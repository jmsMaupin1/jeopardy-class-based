class Grid {
    constructor(options = {
        rowCount: 10,
        columnCount: 10,
        cellWidth: '10px',
        cellHeight: '10px',
    }) {
        this.options = options;
        this.matrix = [];
    }

    createGridElement() {
        this.ele = document.createElement('div');
        this.ele.id = 'grid';
        for (let rowIndex = 0; rowIndex < this.options.rowCount; ++rowIndex) {

            const row = this.createRowElement(rowIndex);
            this.ele.appendChild(row);
            this.matrix[rowIndex] = []

            for (let cellIndex = 0; cellIndex < this.options.columnCount; ++cellIndex) {
                
                const cell = this.createCellInstance();
                row.appendChild(cell.createCellElement(rowIndex, cellIndex));
                this.matrix[rowIndex][cellIndex] = cell;
            }
        }

        return this.ele;
    }

    createCellInstance() {
        return new Cell(this.options)
    }

    createRowElement(rowIndex) {
        const row = document.createElement('div');
        row.classList.add('grid-row');
        row.dataset.rowIndex = rowIndex;

        return row;
    }
    
    addEventListeners(eventDescriptions) {
        for(let {listener, type, callback} of eventDescriptions) {
            listener.addEventListener(type, callback);
        }        
    }
}