class JeopardyCell extends Cell {
    constructor(options, value = 0, question = "", answer = "") {
        super(options);
        this.value = value;
        this.question = question;
        this.answer = answer;
    }

    isEmpty() {
        return this.value !== "" && this.question !== "" && this.answer !== "";
    }
}