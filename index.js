const BASE_URL = 'http://kenziejservice.herokuapp.com/api'
const gridContainer = document.getElementById('grid-container');

const grid = new JeopardyGrid({
    rowCount: 6,
    columnCount: 6,
    cellWidth: '150px',
    cellHeight: '150px',
    cellClasses: ['text-cell'],    
})

const gridElement = grid.createGridElement();
gridContainer.appendChild(gridElement);

startGame( questions => {
    if(!questions) return;

    let categories = Object.keys(questions);

    for (let cellIndex = 0; cellIndex < categories.length; cellIndex++) {
        let clues = questions[categories[cellIndex]];
        grid.addCategoryHeader(cellIndex, categories[cellIndex])
        for(let rowIndex = 0; rowIndex < clues.length; rowIndex++) {
            grid.addQuestionsToCell({
                    rowIndex: rowIndex + 1, 
                    cellIndex: cellIndex, 
                    value: (rowIndex + 1) * 100, 
                    question: clues[rowIndex].question, 
                    answer: clues[rowIndex].answer
                }
            )
        }
    }

    document.getElementById('loading').style.display = 'none';
    document.querySelector('#qna > .modal-content').addEventListener('click', e => e.stopPropagation());
    document.getElementById('qna').addEventListener('click', e => {
        console.log(e.target);
        document.getElementById('qna').style.display = 'none';
    })
    document.getElementById('answer').addEventListener('keypress', e => {
        if (e.keyCode !== 13) return;

        grid.checkAnswer(answer.value);
        document.getElementById('qna').style.display = 'none';
    })
});

const eventListeners = [
    new EventDescription({
        listener: gridElement,
        type: 'click',
        callback: grid.chooseClue,
        context: grid
    })
];

grid.addEventListeners(eventListeners);