function randBetween(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function getAllCategories(callback) {
    fetch(`${BASE_URL}/categories`)
        .then(res => res.json())
        .then(json => callback(json))
        .catch(console.log);
}

function getCluesByCategoryID(category_id) {
    return fetch(`${BASE_URL}/clues?category=${category_id}`)
        .then(res => res.json());
}

function prepareData(data) {
    let clues =  deleteInvalidClues(data);
    clues = deleteCategoriesWithLessThanFiveClues(clues);
    clues = chooseNUniqueElementsFromASet(clues, 6);
    for(clue in clues) {
        if(clues[clue].length > 5)
            clues[clue] = chooseNUniqueElementsFromASet(clues[clue], 5)
    }

    return clues;
   
}

function deleteCategoriesWithLessThanFiveClues(clues) {
    let categories = Object.keys(clues)

    for(let category of categories) {
        if(clues[category].length < 5) {
            delete clues[category]
        }
    }
    
    return clues;
}

function deleteInvalidClues(clues) {
    for (let key in clues) {
        clues[key] = clues[key].filter(clue => !clue.invalidCount)
    }
    return clues;
}

function chooseNUniqueElementsFromASet(set, n) {
    let uniqueElements = [];

    if(!Array.isArray(set)) {
        if(!Object.keys(set).length) return null;

        let keys = Object.keys(set);
        while(uniqueElements.length < n) {
            let keyIndex = randBetween(0, keys.length - 1);
            let element = JSON.stringify(set[keys[keyIndex]]);

            if (uniqueElements.indexOf(element) === -1) 
                uniqueElements.push([keys[keyIndex], element]);
        }

        uniqueElements = Object.fromEntries(
            uniqueElements.map(([key, element]) => {
                return [key, JSON.parse(element)];
            })
        );
    } else {
        if(set.length < n) return null;

        while(uniqueElements.length < n) {
            let element = set[randBetween(0, set.length - 1)];
            if(uniqueElements.indexOf(element) === -1) uniqueElements.push(element);
        }
    }
    
    return uniqueElements
}

function startGame(callback) {
    getAllCategories(json => {
        let numOfCategories = json.categories.length;
        let categories = [];
        let clues = [];
    
        for (let i = 0; i < 100; ++i) {
            let rand = randBetween(0, numOfCategories);
            let category = json.categories[rand];
    
            if(categories.indexOf(category) === -1)
                categories.push(category);
            else i--;
        }
    
        for (let category of categories)  {
            clues.push(getCluesByCategoryID(category.id));
        }

        Promise.all(clues)
            .then(clue => {
                let cluesByCategory = clue.reduce( (pre, cur) => {
                    let title = cur.clues[0].category.title;
                    pre[title] = cur.clues;
                    return pre;
                }, {})
                
                callback(prepareData(cluesByCategory));
    
            }
        ).catch(console.log)
    })
}